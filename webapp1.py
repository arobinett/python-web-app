# Deploys a simple website with Flash web framework

# Flask contains prototype objects
from flask import Flask, render_template

# store flask web object in app, creating an instance
app = Flask(__name__)

# route is Flask method for defining the path where an file or app resides
# the decorator 'app.route' is wrapper function enhancing the base app object instance
@app.route('/') # '/' is the path to the home page of the website in this instance
def home():
    """This function simply defines the content present on the home page"""
    return render_template("home.html") # by default, render_template expects the "templates" directory to exist in the same directory as the running app.

@app.route('/about') # '/' is the path to the home page of the website in this instance
def about():
    """This function simply defines the content present on the about page"""
    return render_template("about.html")

if __name__=="__main__":
    app.run(debug=True)

